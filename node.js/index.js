require('dotenv').config()


const Database = require('./config/database')


class testDBTransction {
    constructor () {
        this.database = new Database().getInstance() 
    }

    getUser () {
       const users = this.database.from('User').whereNot('id', null)
       return users
    }

    async createUser () {
       const [id] = await this.database.insert({name: 'budi', age: 24 }).into('User')
       return id

    }

    async createArticleWithUser () {
        const t =  await this.database.transaction()
        
        try {
          const [id] = await this.database.insert({name: 'budi', age: 25 }).transacting(t).into('User')
          await this.database.insert({title: 'book 1', user_id: id }).transacting(t).into('Article')
          t.commit()  
          console.log('done without error')
        } catch (error) {
          console.log(error)
          t.rollback()  
        }

    }


}

const t = new testDBTransction()
// t.createUser().then(() => {
//     t.getUser().then(result => {
//         console.log(result)
//     }) 
// })

t.createArticleWithUser().then(()=>{
    console.log('done')
})