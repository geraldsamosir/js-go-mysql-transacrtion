package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	// "gitlab.com/geraldsamosir/go-api-run.git/src/config/database"
)

type DatabaseInstance struct {
	hostName           string
	user               string
	password           string
	dbName             string
	SetConnMaxIdleTime int
	SetConnMaxLifetime int
}

type DB struct {
	*sqlx.DB
}

func (di *DatabaseInstance) GetConnection() *DB {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	di.dbName = os.Getenv("DATABASE_NAME")
	di.user = os.Getenv("DATABASE_USER")
	di.hostName = os.Getenv("DATABASE_HOST")
	di.password = os.Getenv("DATABASE_PASSWORD")
	di.SetConnMaxIdleTime = 2
	di.SetConnMaxLifetime = 1

	db, err := sqlx.Open("mysql", fmt.Sprintf("%s:%s@tcp(localhost:3306)/%s?parseTime=true", di.user, di.password, di.dbName))

	if err != nil {
		panic(err)
	}

	db.SetMaxIdleConns(10)
	db.SetMaxOpenConns(100)
	db.SetConnMaxIdleTime(time.Duration(di.SetConnMaxIdleTime) * time.Minute)
	db.SetConnMaxLifetime(time.Duration(di.SetConnMaxLifetime) * time.Minute)

	return &DB{db}

}

type Article struct {
	Id     int    `db:"id"`
	Title  string `db:"title"`
	UserId int    `db:"user_id"`
}

type User struct {
	Id   int    `db:"id"`
	name string `db:"name"`
	age  int    `db: "age"`
}

func main() {
	var db DatabaseInstance
	connection := db.GetConnection()

	articles := []Article{}
	err := connection.Select(&articles, "select id, title, user_id from Article")

	if err != nil {
		fmt.Println("articleRepo FindAll", err.Error())
		//return []entity.ArticleEntityResponse{}, err
	}

	ctx := context.Background()

	/// create transaction

	fail := func(err error) (int64, error) {
		return 0, fmt.Errorf("CreateOrder: %v", err)
	}

	tx, err := connection.BeginTx(ctx, nil)

	if err != nil {
		fail(err)
	}

	defer tx.Rollback()

	/// create users
	var user User

	user.age = 21
	user.name = "budi"

	users, err := tx.ExecContext(ctx, `
	insert into User (name,age)
	values(?, ? )`, user.name, user.age)

	if err != nil {
		fmt.Println("userRepo  Create", err.Error())
	}

	userId, _ := users.LastInsertId()

	fmt.Println(userId)

	/// create article

	article, err := tx.ExecContext(ctx, `
	  insert into Article (title, user_id)
	  values(?, ? )`, "title transaction", userId)

	if err != nil {
		fmt.Println("articleRepo  Create", err.Error())
	}

	articleID, _ := article.LastInsertId()

	fmt.Println("success", articleID)

	if err = tx.Commit(); err != nil {
		fail(err)
	}

}
